import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../_service/authentication.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    submitted: boolean = false;
    loadingIcon: boolean = false;
    profileUrl: string = '/profile';
    error = '';


    constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthenticationService) { }


    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required]],
            password: ['', Validators.required]
        });

        // logout user to prevent double login
        this.authService.logout();

    }


    onSubmit() {
        
        this.submitted = true;

        // return if form is invalid
        if (this.loginForm.invalid) {
            this.error = '';
            return;
        }

        this.loadingIcon = true;

        // Authenticate user
        this.authService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value).subscribe(
            data => {
                //console.log(data);
                this.router.navigate([this.profileUrl]);
            },
            error => {
                //console.log(error);
                this.loadingIcon = false;
                this.error = error.error.message;
            });
    }


}