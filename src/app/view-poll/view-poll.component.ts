import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators} from '@angular/forms';
import { Chart } from 'chart.js';
import { PollDataService } from '../_service/poll-data.service';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-view-poll',
  templateUrl: './view-poll.component.html',
  styleUrls: ['./view-poll.component.scss']
})
export class ViewPollComponent implements OnInit {

    pollForm: FormGroup = null;
    poll_id: string = "";
    acquire_poll: boolean = false;
    voted: boolean = false;
    counter = null;
    poll_question = null;
    poll_choices = null;
    chart = null;
    chartColors = [];
    
    constructor(private fb: FormBuilder, private elementRef: ElementRef, 
        private route: ActivatedRoute, private pollService: PollDataService,
        private socket: Socket) {

        // Acquire poll id pass in the url /poll/:id
        this.route.params.subscribe(params => this.poll_id = params.id);
    }

    // Fetch the poll status
    ngOnInit() {
        this.updatePoll();
        this.socket.on("message", data => console.log(data));
        this.socket.on("update_poll", data => this.socketUpdatePoll(data));
    }

    // get poll info and update current status
    updatePoll() {
        this.pollService.getPollResult(this.poll_id).subscribe(data =>{
            this.poll_question = data['question'];
            // Keeps the graph labels/totals in order
            this.poll_choices = data['choices'].sort((n1, n2) => n1['text'] > n2['text']);
            this.acquire_poll = true;

            if (this.voted) {
                this.socket.connect();
                this.socket.emit('join', {'room': this.poll_id});
                //this.displayChart("canvas-pie");
                this.displayChart("canvas-bar");

            } else {
                this.createVoteForm();
            }

        },
        error => {
            console.log('invalid poll url');
        });
    }

    socketUpdatePoll(data) {
        this.poll_question = data['question'];
        this.poll_choices = data['choices'].sort((n1, n2) => n1['text'] > n2['text']);
        this.updateResultsChart();
    }



    // close socket and room.
    ngOnDestroy() {
        this.socket.emit('leave', {'room': this.poll_id});       
        this.socket.disconnect();
    }


    createVoteForm() {
        this.pollForm = this.fb.group({
            question: this.poll_question['text'],
            choice: ['', Validators.required]
        });
    }

    // submit vote
    submit(form: FormGroup) {

        if (form.invalid)
            return;

        let question_id = this.poll_question['id'];
        let choice_id = form.controls.choice.value;
        this.pollService.vote(this.poll_id, question_id, choice_id).subscribe(res => console.log(res));
        this.voted = true;
        this.updatePoll();

    }

    // view results instead of vote
    viewResults() {
        this.voted = true;
        this.updatePoll();
    }


    // Create and display chart of poll's results
    displayChart(canvasID: string) {
        const canvas = <HTMLCanvasElement> document.getElementById(canvasID);
        const ctx = canvas.getContext("2d");

        let labels = this.poll_choices.map(p => p['text']);
        let totals = this.poll_choices.map(p => p['total']);

        
        if (this.chartColors) {
            for (let i = 0; i < totals.length; i++) {
                this.chartColors.push(this.getRandomColorHex());
            }
        }
        
        let data = {
            labels: labels,
            datasets: [
            {
                //label:"totals",
                fill: true,
                backgroundColor: this.chartColors,
                data: totals,
                borderColor: this.chartColors
            }
            ]
        };

        // display pie chart
        if (canvasID === 'canvas-pie') {

            let options = {
                title: {
                    display: true,
                    text: this.poll_question['text'],
                    position: 'top'
                },
                rotation: -0.7 * Math.PI,
                animation: {
                    animateRotate: true,
                    animateScale: false
                }
            };

            this.chart = new Chart(ctx, {
                type: 'pie',
                data: data,
                options: options
            });


        // display horizontal bar graph    
        } else {

            let options = {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color:'#1B1B1E'
                        },
                        ticks: {
                            fontColor:'#1B1B1E',
                            fontSize:20,
                            beginAtZero: true
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            color:'#1B1B1E'
                        },
                        ticks: {
                            fontColor:'#1B1B1E',
                            fontSize:20
                        }
                    }]
                }
            };

            this.chart = new Chart(ctx, {
                type: 'horizontalBar',
                data: data,
                options: options
            });

        }
    }

    
    getRandomColorHex() {
        const hex = "0123456789ABCDEF";
        let color = "#";

        for (let i = 1; i <= 6; i++) {
            color += hex[Math.floor(Math.random() * 16)];
        }

        return color;
    }


    updateResultsChart() {
        let totals = this.poll_choices.map(p => p['total']);
        this.chart.data.datasets[0].data = totals;

        this.chart.update();
    }

}
