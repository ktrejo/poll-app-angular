import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, interval } from 'rxjs';
import { PollDataService } from '../_service/poll-data.service';
import { AuthenticationService } from '../_service/authentication.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    polls;


    constructor(private router: Router, private pollService: PollDataService, private authService: AuthenticationService) { }


    ngOnInit() {

        if (!this.authService.userSignIn())
            this.router.navigate(['/login']);

        this.getUserPolls();
    }


    logout() {

        this.authService.logout();
        this.router.navigate(['/login']);
    }


    getUserPolls() {

        this.pollService.userPolls().subscribe(polls => {
            this.polls = polls['polls'];
            //console.log(this.polls);
        });
    }
}
