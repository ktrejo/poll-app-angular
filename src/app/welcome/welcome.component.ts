import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_service/authentication.service';


@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

    searchPoll: FormGroup;
    user: string


    constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthenticationService) { 
        this.user = 'Login to Create a Poll';
        this.searchPoll = this.formBuilder.group({
            pollId: ['', Validators.required]
        });
    }


    ngOnInit() {
        if (this.authService.userSignIn()) {
            this.user = 'View Profile';
        }
    }


    onSubmit() {

        //return if search form is blank
        if (this.searchPoll.invalid) {
            return;
        }

        const pollId = this.searchPoll.controls['pollId'].value;
        this.router.navigate(['/poll', pollId]);
    }

}
