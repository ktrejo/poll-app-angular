import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../_service/authentication.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    registerForm: FormGroup;
    submitted: boolean = false;
    loadingIcon: boolean = false;
    profileUrl: string = '/profile';
    error = '';


    constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthenticationService) { }


    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            email: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // logout user to prevent double register
        this.authService.logout();

    }


    onSubmit() {
        this.submitted = true;

        // return if form is invalid
        if (this.registerForm.invalid) {
            this.error = '';
            return;
        }

        this.loadingIcon = true;


        this.authService.register(this.registerForm.controls.email.value, this.registerForm.controls.username.value, 
            this.registerForm.controls.password.value).subscribe(
                data => {
                    console.log(data);
                    this.router.navigate([this.profileUrl]);
                },
                error => {
                    this.loadingIcon = false;
                    this.error = error.error.message;
                    console.log(this.error);
                });
    }
}