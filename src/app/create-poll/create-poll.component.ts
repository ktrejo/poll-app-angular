import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormArray, FormBuilder, Validators} from '@angular/forms';
import { map } from 'rxjs/operators';
import { PollDataService } from '../_service/poll-data.service';
import { AuthenticationService } from '../_service/authentication.service';

@Component({
    selector: 'app-create-poll',
    templateUrl: './create-poll.component.html',
    styleUrls: ['./create-poll.component.scss']
})
export class CreatePollComponent implements OnInit {

    pollForm: FormGroup;

    constructor(private fb: FormBuilder, private router: Router, private pollService: PollDataService, private authService: AuthenticationService) { }


    ngOnInit() {

        this.pollForm = this.fb.group({
            question: ['', Validators.required],
            choices: this.fb.array([this.createChoice()])

        });
    }


    createChoice() {

        // create new formcontrol
        return this.fb.group({
            choice: ['', [Validators.required]]
        });
    }


    addChoice() {

        // Add new choice input
        const controls = <FormArray>this.pollForm.controls['choices'];

        controls.push(this.createChoice());
    }


    removeChoice(i: number) {

        //remove choice input
        const controls = <FormArray>this.pollForm.controls['choices'];

        controls.removeAt(i);
    }

    cancelForm() {
        this.router.navigate(['/profile']);
    }


    onSubmit(form: FormGroup) {

        if (this.pollForm.invalid) {
            console.log("invalid");
            return;
        }

        console.log('for received');

        const question = form.controls['question'].value;
        const formArray = <FormArray>form.controls['choices'];
        const choices = formArray.controls.map(fg => (<FormGroup>fg).controls['choice'].value);

        const payload = {
            question: question,
            choices: choices
        }

        console.log(payload);
        this.pollService.createPoll(question, choices).subscribe(res => {
            this.router.navigate(['/poll', res['poll_url']])
            console.log(res);
        });
    }

}
