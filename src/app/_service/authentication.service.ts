import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    api_url: string

    constructor(private http: HttpClient) {
        this.api_url='http://127.0.0.1:5000/api/v1/';  
    }

    // register new user
    register(email: string, username: string, password: string) {
        
        const data = {
            "email": email,
            "username": username,
            "password": password
        };

        // when subscribe to, store the access token and refresh token in sessiong storage
        return this.http.post(this.api_url + 'registration', data)
            .pipe(map(user => {

                if (user && user['access_token']) {
                    sessionStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            }));         
    }

    // login user
    login(username: string, password: string) {

        const data = {
            "username": username,
            "password": password
        };

        return this.http.post(this.api_url + 'login', data)
            .pipe(map(user => {
                // if login is success, store access and refresh token
                if (user && user['access_token']) {
                    sessionStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            }));
    }



    getAccessToken() {
        
        const user = sessionStorage.getItem('currentUser');

        if (user === '' || user === null)
            return "";

        return JSON.parse(user)['access_token'];
    }


    getRefreshToken() {

        const user = sessionStorage.getItem('currentUser');

        if (user === '' || user === null)
            return "";

        return JSON.parse(user)['refresh_token'];
    }


    userSignIn() {

        const user = sessionStorage.getItem('currentUser');

        if (user === '' || user === null)
            return false;

        return true;
    }
    

    refreshAccessToken() {

        // Revoke access and refresh token and remove from sessiong storage
        const cred = JSON.parse(sessionStorage.getItem('currentUser'));

        const data = {};
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cred.refresh_token
            })
        };

        return this.http.post(this.api_url + 'token/refresh', data, httpOptions)
            .pipe(map(user => {

                if (user && user['access_token']) {
                    let currentUser = JSON.parse(sessionStorage.getItem('currentUser'));

                    // refresh token api only returns new access token unlike login
                    currentUser['access_token'] = user['access_token'];
                    sessionStorage.setItem('currentUser', JSON.stringify(currentUser));
                }

                // return access token string for interceptor to retry request
                return user['access_token'];
            }));
    }


    logout(){
        // Revoke access and refresh token and remove from sessiong storage
        const user = sessionStorage.getItem('currentUser');

        // if logout is called before login
        if (user === null || user === '')
            return;

        const cred = JSON.parse(user);
        this.revokeToken(cred.access_token, 'access');
        this.revokeToken(cred.refresh_token, 'refresh');

        sessionStorage.removeItem('currentUser');

    }

    
    revokeToken(token: string, action: string) {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            })
        };

        const data = {};

        this.http.post(this.api_url + 'logout/' + action, data, httpOptions).subscribe(data => console.log(action + " removed"));
    }
}
