import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) { }

  canActivate() {

      if (sessionStorage.getItem('currentUser')) {
          // logged in
          return true;
      }

      //not logged in so redirect to login page
      this.router.navigate(['/login']); 

      return false;

  }
}
