import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class PollDataService {

    api_url: string


    constructor(private http: HttpClient) {
        this.api_url = 'http://127.0.0.1:5000/api/v1/poll/'
    }

    // returns observable object for the poll result
    getPollResult(url: string) {

        return this.http.get(this.api_url + 'result/' +url);
    }

    // return observable object for creating poll
    createPoll(question, choices) {

        // Need to past authentication
        const data = {
            "question": question,
            "choices": choices
        };

        return this.http.post(this.api_url + 'create', data);
    }

    // return observable object for listing all polls
    userPolls() {

        return this.http.get(this.api_url + 'polls');
    }

    // return observable object for voting
    vote(question_url, question_id, choice_id) {

        const data = {
            'question_url': question_url,
            'question_id': question_id,
            'choice_id': choice_id
        };

        return this.http.post(this.api_url + 'update', data);
    }
}
